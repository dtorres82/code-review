package com.idealista.api.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.util.Date;

@Getter
@Setter
@SuperBuilder
public class IrrelevantAdVO extends PublicAdVO {
    private Date irrelevantSince;
}
