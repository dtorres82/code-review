package com.idealista.api.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
public class QualityAdVO extends PublicAdVO {
    private Integer score;
}
