package com.idealista.api.converters;

import com.idealista.api.dto.IrrelevantAdVO;
import com.idealista.api.dto.PublicAdVO;
import com.idealista.api.dto.QualityAdVO;
import com.idealista.infrastructure.domain.model.Ad;
import com.idealista.infrastructure.domain.model.Picture;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
public class AdConverter {

    public PublicAdVO convertToPublicAdVO(Ad ad) {
        return PublicAdVO.builder()
                .id(ad.getId())
                .typology(ad.getTypology().name())
                .description(ad.getDescription())
                .pictureUrls(ad.getPictures().stream().map(Picture::getUrl).collect(Collectors.toList()))
                .houseSize(ad.getHouseSize())
                .gardenSize(ad.getGardenSize())
                .build();
    }

    public QualityAdVO convertToQualityAdVO(Ad ad) {
        return QualityAdVO.builder()
                .id(ad.getId())
                .typology(ad.getTypology().name())
                .description(ad.getDescription())
                .pictureUrls(ad.getPictures().stream().map(Picture::getUrl).collect(Collectors.toList()))
                .houseSize(ad.getHouseSize())
                .gardenSize(ad.getGardenSize())
                .score(ad.getScore())
                .build();
    }

    public IrrelevantAdVO convertToIrrelevantAdVO(Ad ad) {
        return IrrelevantAdVO.builder()
                .id(ad.getId())
                .typology(ad.getTypology().name())
                .description(ad.getDescription())
                .pictureUrls(ad.getPictures().stream().map(Picture::getUrl).collect(Collectors.toList()))
                .houseSize(ad.getHouseSize())
                .gardenSize(ad.getGardenSize())
                .irrelevantSince(ad.getIrrelevantSince())
                .build();
    }

}
