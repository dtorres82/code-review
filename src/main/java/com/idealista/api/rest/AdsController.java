package com.idealista.api.rest;

import com.idealista.api.converters.AdConverter;
import com.idealista.api.dto.IrrelevantAdVO;
import com.idealista.api.dto.PublicAdVO;
import com.idealista.api.dto.QualityAdVO;
import com.idealista.infrastructure.ports.primary.AdsCalculateService;
import com.idealista.infrastructure.ports.primary.AdsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/ads")
public class AdsController {

    @Autowired
    private AdsService adsService;
    @Autowired
    private AdsCalculateService adsCalculateService;

    @Autowired
    private AdConverter adConverter;

    // Endpoint para listar todos lo anuncios que son de cumplen los requisitos de calidad minima requerida
    @GetMapping("/quality")
    public ResponseEntity<List<QualityAdVO>> qualityListing() {
        return ResponseEntity.ok(
                adsService.findQualityAds()
                        .stream()
                        .map(adConverter::convertToQualityAdVO)
                        .collect(Collectors.toList())
        );
    }

    // Endpoint para listar todos lo anuncios que son irrelevantes
    @GetMapping("/irrelevant")
    public ResponseEntity<List<IrrelevantAdVO>> irrelevantListing() {
        return ResponseEntity.ok(
                adsService.findIrrelevantAds()
                        .stream()
                        .map(adConverter::convertToIrrelevantAdVO)
                        .collect(Collectors.toList())
        );
    }
    // Endpoint para listar todos lo anuncios existentes
    @GetMapping("/public")
    public ResponseEntity<List<PublicAdVO>> publicListing() {
        return ResponseEntity.ok(
                adsService.findPublicAds()
                        .stream()
                        .map(adConverter::convertToPublicAdVO)
                        .collect(Collectors.toList())
        );
    }

    // Endpoint para poder calcular todos los anuncios
    @GetMapping("/score")
    public ResponseEntity<Void> calculateScore() {
        adsCalculateService.calculateAllAdsScores();
        return ResponseEntity.accepted().build();
    }
}
