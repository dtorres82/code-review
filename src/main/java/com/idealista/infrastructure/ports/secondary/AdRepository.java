package com.idealista.infrastructure.ports.secondary;

import com.idealista.infrastructure.domain.model.Ad;

import java.util.List;

public interface AdRepository {
    List<Ad> findAllAds();

    void save(Ad ad);
}
