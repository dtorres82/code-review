package com.idealista.infrastructure.ports.primary;

import com.idealista.infrastructure.domain.model.Ad;

public interface AdsCalculateService {
    void calculateAllAdsScores();

    Ad calculateAdScore(Ad ad);
}
