package com.idealista.infrastructure.ports.primary;

import com.idealista.infrastructure.domain.model.Ad;

import java.util.List;

public interface AdsService {

    List<Ad> findPublicAds();

    List<Ad> findQualityAds();

    List<Ad> findIrrelevantAds();

}
