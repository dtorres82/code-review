package com.idealista.infrastructure.domain.enums;

public enum Quality {
    HD,
    SD,
}
