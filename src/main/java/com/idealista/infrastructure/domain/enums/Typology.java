package com.idealista.infrastructure.domain.enums;

public enum Typology {
    FLAT,
    CHALET,
    GARAGE,
}
