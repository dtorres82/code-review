package com.idealista.infrastructure.domain.model;

import com.idealista.infrastructure.domain.enums.Quality;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Picture {
    private Integer id;
    private String url;
    private Quality quality;
}
