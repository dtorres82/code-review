package com.idealista.infrastructure.adapters.secondary.vo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PictureVO {
    private Integer id;
    private String url;
    private String quality;
}
