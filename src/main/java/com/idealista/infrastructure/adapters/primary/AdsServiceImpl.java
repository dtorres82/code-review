package com.idealista.infrastructure.adapters.primary;

import com.idealista.infrastructure.domain.constants.Constants;
import com.idealista.infrastructure.domain.model.Ad;
import com.idealista.infrastructure.ports.primary.AdsCalculateService;
import com.idealista.infrastructure.ports.primary.AdsService;
import com.idealista.infrastructure.ports.secondary.AdRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Service
public class AdsServiceImpl implements AdsService {

    private final Predicate<Ad> scoreNotCalculated = ad -> Objects.isNull(ad.getScore());
    private final Predicate<Ad> relevantAds = ad -> ad.getScore() >= Constants.FORTY;
    private final Predicate<Ad> irrelevantAds = ad -> ad.getScore() < Constants.FORTY;
    @Autowired
    private AdRepository adRepository;
    @Autowired
    private AdsCalculateService adsCalculateService;

    @Override
    public List<Ad> findPublicAds() {

        List<Ad> adList = adRepository.findAllAds();

        return checkAdsScoreIsCalculate(adList);
    }

    @Override
    public List<Ad> findQualityAds() {
        List<Ad> adList = adRepository.findAllAds();

        return checkAdsScoreIsCalculate(adList)
                .stream()
                .filter(relevantAds)
                .sorted(Comparator.comparing(Ad::getScore).reversed())
                .collect(Collectors.toList());
    }

    @Override
    public List<Ad> findIrrelevantAds() {
        List<Ad> adList = adRepository.findAllAds();

        return checkAdsScoreIsCalculate(adList)
                .stream()
                .filter(irrelevantAds)
                .collect(Collectors.toList());
    }

    private List<Ad> checkAdsScoreIsCalculate(List<Ad> adList) {
        adList.stream()
                .filter(scoreNotCalculated)
                .forEach(ad -> adsCalculateService.calculateAdScore(ad));

        return adList;
    }

}
