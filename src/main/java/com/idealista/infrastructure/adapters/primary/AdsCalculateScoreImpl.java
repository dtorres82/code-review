package com.idealista.infrastructure.adapters.primary;

import com.idealista.infrastructure.domain.constants.Constants;
import com.idealista.infrastructure.domain.enums.Quality;
import com.idealista.infrastructure.domain.enums.Typology;
import com.idealista.infrastructure.domain.model.Ad;
import com.idealista.infrastructure.ports.primary.AdsCalculateService;
import com.idealista.infrastructure.ports.secondary.AdRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class AdsCalculateScoreImpl implements AdsCalculateService {

    @Autowired
    private AdRepository adRepository;

    @Override
    public void calculateAllAdsScores() {
        adRepository
                .findAllAds()
                .forEach(this::calculateScore);
    }

    @Override
    public Ad calculateAdScore(Ad ad) {
        calculateScore(ad);
        return ad;
    }

    private void calculateScore(Ad ad) {
        int score = Constants.ZERO;

        score += getScoreByPictures(ad);

        score += getScoreByDescription(ad);

        //Calcular puntuación por completitud

        if (ad.isComplete()) {
            score += Constants.FORTY;
        }

        getFinalScore(ad, score);

        if (ad.getScore() < Constants.FORTY) {
            ad.setIrrelevantSince(new Date());
        } else {
            ad.setIrrelevantSince(null);
        }

        adRepository.save(ad);
    }

    private void getFinalScore(Ad ad, int score) {
        if (score < Constants.ZERO) {
            ad.setScore(Constants.ZERO);
        } else {
            ad.setScore(Math.min(score, Constants.ONE_HUNDRED));
        }

    }

    private int getScoreByDescription(Ad ad) {

        //Calcular puntuación por descripción
        int score = Constants.ZERO;
        Optional<String> optDesc = Optional.ofNullable(ad.getDescription());

        if (optDesc.isPresent()) {
            String description = optDesc.get();

            if (!description.isEmpty()) {
                score += Constants.FIVE;
            }

            List<String> wds = Arrays.asList(description.split(" ")); //número de palabras
            if (Typology.FLAT.equals(ad.getTypology())) {
                if (wds.size() >= Constants.TWENTY && wds.size() <= Constants.FORTY_NINE) {
                    score += Constants.TEN;
                }

                if (wds.size() >= Constants.FIFTY) {
                    score += Constants.THIRTY;
                }
            }

            if (Typology.CHALET.equals(ad.getTypology()) && wds.size() >= Constants.FIFTY) {
                score += Constants.TWENTY;
            }

            if (wds.contains("luminoso")) score += Constants.FIVE;
            if (wds.contains("nuevo")) score += Constants.FIVE;
            if (wds.contains("céntrico")) score += Constants.FIVE;
            if (wds.contains("reformado")) score += Constants.FIVE;
            if (wds.contains("ático")) score += Constants.FIVE;
        }

        return score;
    }

    private int getScoreByPictures(Ad ad) {
        int score = Constants.ZERO;

        //Calcular puntuación por fotos
        if (ad.getPictures().isEmpty()) {
            score -= Constants.TEN; //Si no hay fotos restamos 10 puntos
        } else {
            score += Constants.TWENTY * ad.getPictures().stream().filter(picture -> Quality.HD.equals(picture.getQuality())).count();
            score += Constants.TEN * ad.getPictures().stream().filter(picture -> Quality.SD.equals(picture.getQuality())).count();
        }
        return score;
    }
}
